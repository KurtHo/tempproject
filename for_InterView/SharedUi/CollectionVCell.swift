//
//  CollectionVCell.swift
//  for_InterView
//
//  Created by KurtHo on 2019/9/15.
//  Copyright © 2019 KurtHo. All rights reserved.
//

import UIKit

class CollectionVCell: UICollectionViewCell {
    @IBOutlet weak var imageView: MyImageView!
    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var title: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.backgroundColor = .white
        self.layer.borderWidth = 0.5
    }
    
    override func prepareForReuse() {
        self.imageView.image = nil
        self.id.text = nil
        self.title.text = nil
    }
    
    func contentSets(id: String, title: String, image: UIImage? ){
        self.id.text            = id
        self.title.text         = title
        self.imageView.image    = image ?? UIImage()
    }

}
