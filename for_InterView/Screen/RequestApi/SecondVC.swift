//
//  SecondVC.swift
//  for_InterView
//
//  Created by KurtHo on 2019/9/15.
//  Copyright © 2019 KurtHo. All rights reserved.
//

import UIKit

class SecondVC: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    let vm = SecondVM()
    let screen = UIScreen.main.bounds
    var collectionData: [TempModule]? {
        didSet{
            self.collectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewSets()
    }
    
    func collectionViewSets(){
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        collectionView.register(CollectionVCell.nib, forCellWithReuseIdentifier: CollectionVCell.identifier)
    }
    
}


extension SecondVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("index: ", indexPath.row)
    }
}

extension SecondVC: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return (collectionData != nil) ? collectionData!.count : 10
        return collectionData?.count ?? 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionVCell.identifier, for: indexPath) as! CollectionVCell
        
        if let data = collectionData, let id = data[indexPath.row].id, let title = data[indexPath.row].title {
            cell.imageView.str = data[indexPath.row].thumbnailUrl?.absoluteString
            
            Api.downloadImage(urlStr: (data[indexPath.row].thumbnailUrl)?.absoluteString, defaultImage: nil) { (img, str) in
                
                DispatchQueue.main.async {
//                    if cell.imageView.image == nil && cell.imageView.str == str {
                    if cell.imageView.str == str {
                        cell.contentSets(id: String(id), title: title, image: img )
                    }
                }
            }
        }
        
        return cell
    }
    
}

extension SecondVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width / 4
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
