//
//  ViewController.swift
//  for_InterView
//
//  Created by KurtHo on 2019/9/15.
//  Copyright © 2019 KurtHo. All rights reserved.
//

import UIKit

class FirstVC: UIViewController {
    @IBOutlet weak var myLabel: UILabel!
    let vm = FirstVM()
    let urlStr = "https://jsonplaceholder.typicode.com/photos"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myLabel.text = "JSON Place Holder"
        print(UIScreen.main.bounds)
    }
    
    @IBAction func requestApi(_ sender: Any) {
        let vc = InstantiaVC.viewController(InstantiaVC.RequestApi.secondVc) as! SecondVC

        vm.getData(urlStr){ data in
            print("my data: \(data.count)")
            DispatchQueue.main.async {
                vc.collectionData = data
            }
            
        }
        
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    
}
