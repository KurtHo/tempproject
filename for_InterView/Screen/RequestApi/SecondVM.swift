//
//  SeconvVM.swift
//  for_InterView
//
//  Created by KurtHo on 2019/9/15.
//  Copyright © 2019 KurtHo. All rights reserved.
//

import UIKit

class SecondVM {
    var collectionData: [TempModule]?
    
    func downloadImage(urlString: String?, defaultImage: UIImage?,  finish: @escaping (UIImage?, String?) -> ()){
        Api.downloadImage(urlStr: urlString, defaultImage: defaultImage) { (img, str) in
            finish(img, str)
        }
    }
    
}
