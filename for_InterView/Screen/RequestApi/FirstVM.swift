//
//  FirstVM.swift
//  for_InterView
//
//  Created by KurtHo on 2019/9/15.
//  Copyright © 2019 KurtHo. All rights reserved.
//

import Foundation

class FirstVM {
    func getData(_ urlStr: String, finish: @escaping ([TempModule]) -> Void){
        Api.getData(urlString: urlStr, finish: finish)
    }
    
}

struct TempModule: Decodable {
    var albumId:        Int?
    var id:             Int?
    var title:          String?
    var url:            URL?
    var thumbnailUrl:   URL?
}
