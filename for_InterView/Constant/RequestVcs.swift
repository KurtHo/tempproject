//
//  RequestVcs.swift
//  for_InterView
//
//  Created by KurtHo on 2019/9/15.
//  Copyright © 2019 KurtHo. All rights reserved.
//

import UIKit

protocol VcMapDelegate {
    var storyBoardId: String {get}
    var identifier: String {get}
}

class InstantiaVC {
    static func viewController(_ vc: VcMapDelegate) -> UIViewController {
        let sb = UIStoryboard(name: vc.storyBoardId, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: vc.identifier)
        return vc
    }
    
    enum RequestApi: VcMapDelegate {
        case firstVc, secondVc
        
        var storyBoardId: String{
            switch self {
            case .firstVc, .secondVc:
                return "Main"
            }
        }
        
        var identifier: String {
            switch self {
            case .firstVc:      return "FirstVC"
            case .secondVc:     return "SecondVC"
            }
        }
    }
}
