//
//  Api.swift
//  for_InterView
//
//  Created by KurtHo on 2019/9/15.
//  Copyright © 2019 KurtHo. All rights reserved.
//

import UIKit

class Api {
    static func getData<T: Decodable>(urlString: String, finish: ( (T)->Void)? = nil ){
        guard let url = URL(string: urlString) else {return}
        let session = URLSession.shared
        session.dataTask(with: url){ data, resp, err in
            if let err = err {
                print("an error has oocur while calling ... \(#function), : \(err)")
            }
            
            if let data = data {
                do {
                    let json = try JSONDecoder().decode(T.self, from: data)
                    finish?(json)
                } catch {
                    print(" err with ...\(#function) \(error.localizedDescription)")
                } 
            }
        }.resume()
        
    }
    
    static func downloadImage(urlStr: String?, defaultImage: UIImage? = nil, finish:  @escaping (UIImage?, String?)->()){
        guard let urlString = urlStr, let url = URL(string: urlString)  else {
            finish(defaultImage, urlStr)
            return
        }
//        guard let url = URL(string: urlString) else {
//            finish(defaultImage, urlStr)
//            return
//        }
        
        if let image = Caches.shared.image.object(forKey: urlString as NSString) {
            finish(image, urlString)
        }else{
            URLSession.shared.dataTask(with: url){ data, resp, err in
                if err != nil {
                    print("\(#function) err: \(String(describing: err))")
                    finish(defaultImage, urlStr)
                }
                guard let data = data, let image = UIImage(data: data) else {
                    finish(defaultImage, urlStr)
                    return
                }
                DispatchQueue.main.async {
                    Caches.shared.image.setObject(image, forKey: urlString as NSString)
                    finish(image, urlString)
                }
                
  
            }.resume()
        }
        
        
    }
}


